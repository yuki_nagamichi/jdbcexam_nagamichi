package turn2;

import java.io.Serializable;

public class ItemVO implements Serializable{

	private String itemId;
	private String itemName;
	private String itemUrl;
	private String itemSize;
	private int price;
	private int quantity;
	private boolean isDelete;

	public ItemVO(String itemId, String itemName, String itemUrl, String itemSize, int price, int quantity,
			boolean isDelete) {
		super();
		this.itemId = itemId;
		this.itemName = itemName;
		this.itemUrl = itemUrl;
		this.itemSize = itemSize;
		this.price = price;
		this.quantity = quantity;
		this.isDelete = isDelete;
	}
	public ItemVO() {
		super();
	}

	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemUrl() {
		return itemUrl;
	}
	public void setItemUrl(String itemUrl) {
		this.itemUrl = itemUrl;
	}
	public String getItemSize() {
		return itemSize;
	}
	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int i) {
		this.price = i;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int i) {
		this.quantity = i;
	}
	public boolean isDelete() {
		return isDelete;
	}
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}




}

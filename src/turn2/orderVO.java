package turn2;

import java.io.Serializable;
import java.time.LocalDate;

public class orderVO implements Serializable {
	private String itemId;
	private String userId;
	private String orderId;
	private int quantity;
	private LocalDate orderDate;
	private boolean is_cancelled;

	public orderVO(String itemId, String userId, String orderId, int quantity, LocalDate orderDate,
			boolean is_cancelled) {
		super();
		this.itemId = itemId;
		this.userId = userId;
		this.orderId = orderId;
		this.quantity = quantity;
		this.orderDate = orderDate;
		this.is_cancelled = is_cancelled;
	}
	public orderVO() {
		super();
	}

	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public LocalDate getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}
	public boolean isIs_cancelled() {
		return is_cancelled;
	}
	public void setIs_cancelled(boolean is_cancelled) {
		this.is_cancelled = is_cancelled;
	}
}

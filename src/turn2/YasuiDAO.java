package turn2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class YasuiDAO {

	public Connection connect() throws SQLException {
		Connection con=null;
		final String url="jdbc:mysql://localhost:3306/yasui?serverTimezone=JST&characterEncoding=UTF-8";
		final String username = "yasui";
		final String password = "password";
		con=DriverManager.getConnection(url, username, password);
		return con;
	}

	public void disconnect(Connection con) throws SQLException {
		if(con!=null) {
			con.close();
		}
	}

	public ArrayList<ItemVO> getAllItem(Connection con) throws SQLException{
		ArrayList<ItemVO> itemList =new ArrayList<ItemVO>();
		final String sql="select i.item_id,i.item_name,i.image_url,i.item_size,i.price,"+ "s.quantity,i.is_delete from items i inner join stocks s" + "on i.item_id = s.item_id;";
		try(PreparedStatement st=con.prepareStatement(sql);
				ResultSet rs=st.executeQuery();
				){
			while(rs.next()) {
				ItemVO item=new ItemVO();
				item.setItemId(rs.getString("item_id"));
				item.setItemName(rs.getString("item_name"));
				item.setItemUrl(rs.getString("item_url"));
				item.setItemSize(rs.getString("item_size"));
				item.setPrice(rs.getInt("prise"));
				item.setQuantity(rs.getInt("quantity"));
				item.setDelete(rs.getBoolean("is_delete"));
				itemList.add(item);
			}
		}
		return itemList;
	}

	public void addItem(Connection con,ItemVO vo) throws SQLException {
		final String sql1=" INSERT INTO items VALUES (?,?,?,?,?,false)";
		final String sql2=" INSERT INTO stocks VALUES (?,?,false)";
		try(PreparedStatement st1=con.prepareStatement(sql1);
				PreparedStatement st2=con.prepareStatement(sql2);){
			st1.setString(1, vo.getItemId());
			st1.setString(2, vo.getItemName());
			st1.setString(3, vo.getItemUrl());
			st1.setString(4, vo.getItemSize());
			st1.setInt(5, vo.getPrice());
			st2.setString(1, vo.getItemId());
			st2.setInt(2, vo.getQuantity());
			st1.executeUpdate();
			st2.executeUpdate();
		}
	}

	public void updateStock(Connection con,String itemId,int quantity) throws SQLException {
		final String sql1="select quantity from stocks where item_id=? for update";
		final String sql2="update stocks set quantity = ? where item_id=?";
		try(PreparedStatement st1=con.prepareStatement(sql1);
				PreparedStatement st2=con.prepareStatement(sql2);){
			st1.setString(1, itemId);
			int oldStock=0;
			try(ResultSet rs=st1.executeQuery()){
				if(rs.next()) {
					oldStock=rs.getInt("quantity");
				}
			}

			if(oldStock>=quantity) {
				st2.setInt(1, oldStock-quantity);
			}
			st2.setString(2, itemId);
			st2.executeUpdate();
		}

	}
	public void insertOrder(Connection con,String userid,String itemId,int quantity) throws SQLException {
		final String sql="insert into orders(item_id,user_id,quantity) values(?,?,?)";
		try(PreparedStatement st=con.prepareStatement(sql);){
			st.setString(1,itemId);
			st.setString(2, userid);
			st.setInt(3, quantity);
			st.executeUpdate();
		}

	}
	public void processOrder(Connection con, String userId,String itemId,int quantity) throws SQLException {
		//トランザクション開始
		try {
			con.setAutoCommit(false);
			//在庫情報を行ロック updateStock
			//在庫のチェックと減算 updateStock
			this.updateStock(con, itemId, quantity);
			//注文情報の挿入　insertorder
			this.insertOrder(con, userId, itemId, quantity);
			//コミット
			con.commit();
		}catch(SQLException e) {
			con.rollback();
			e.printStackTrace();
			throw e;
		}

	}

	public void deleteItem(Connection con,String itemId) throws SQLException {
		final String sql="delete from stocks where item_id = ?;";
		try(PreparedStatement st=con.prepareStatement(sql);){
			st.setString(1,itemId);
			st.executeUpdate();
		}
	}

	public ItemVO searchItem(Connection con,String itemName) throws SQLException {
		final String sql="select* from items where item_name=?;";
		ItemVO item=new ItemVO();
		try(PreparedStatement st=con.prepareStatement(sql);){
			st.setString(1,itemName);
			try(ResultSet rs=st.executeQuery();){
				item.setItemId(rs.getString("item_id"));
				item.setItemName(rs.getString("item_name"));
				item.setItemUrl(rs.getString("item_url"));
				item.setItemSize(rs.getString("item_size"));
				item.setPrice(rs.getInt("prise"));
			}
		}
		return item;


	}

}

package turn1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Exam1_1 {

	public static void main(String[] args) {

		final String url = "jdbc:mysql://localhost/yasui?serverTimezone=JST&characterEncoding=UTF-8";
		final String username = "yasui";
		final String password = "password";
		Connection conn = null;
		Statement stmt=null;
		ResultSet rs = null;

		try {
			conn = DriverManager.getConnection(url, username, password);
			stmt = conn.createStatement();
			String query = "select i.item_id,i.item_name,i.image_url,i.item_size,i.price, "
					+ "s.quantity,i.is_delete from items i inner join stocks s on i.item_id "
					+ "= s.item_id;";
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				int item_id=rs.getInt("item_id");
				String item_name = rs.getString("item_name");
				String image_url = rs.getString("imag"
						+ "e_url");
				String item_size = rs.getString("item_size");
				int price = rs.getInt("price");
				int quantity = rs.getInt("quantity");
				String is_delete = rs.getString("is_delete");
				System.out.println(item_id+item_name+image_url+item_size+price+quantity+is_delete);
			}
		} catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
			e.printStackTrace();
		}finally {
			try {
				if(rs!=null) rs.close();
				if(stmt!=null) stmt.close();
				if(conn!=null) conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}

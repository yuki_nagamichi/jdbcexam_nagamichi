package turn1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Exam1_2 {

	public static void main(String[] args) {
		final String url = "jdbc:mysql://localhost/yasui?serverTimezone=JST&characterEncoding=UTF-8";
		final String username = "yasui";
		final String password = "password";
		Connection conn = null;
		Statement stmt=null;
		ResultSet rs = null;

		try {
			conn = DriverManager.getConnection(url, username, password);
			stmt = conn.createStatement();
			String itemsQuery = "INSERT INTO items VALUES ('00000006','デッキチェア ','/images/eyecatch5.jpg','100x60x70',99999,false);";
			String stocksQuery = "INSERT INTO stocks VALUES ('00000006','5',false);";
			int itemsCount = stmt.executeUpdate(itemsQuery);
			int stocksCount = stmt.executeUpdate(stocksQuery);
		}catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
			e.printStackTrace();
		}finally {
			try {
				if(rs!=null) rs.close();
				if(stmt!=null) stmt.close();
				if(conn!=null) conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}

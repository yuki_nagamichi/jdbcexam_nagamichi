package turn3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import turn2.YasuiDAO;

public class Exam3_1 {

	public static void main(String[] args) throws SQLException {

		//商品IDは現在DBにある商品IDよりも大きな値を自動的に 割り振るようにするのやり方わからない
		//在庫も？
		String itemsItemId="00000001";
		String itemName="";
		String itemUrl="";
		String itemSize="50";
		int price=5000;
		String StocksItemId="";
		int quantity=5;
		YasuiDAO dao=new YasuiDAO();

		try(Connection con = dao.connect()) {
			//①商品ID・商品名が既登録のやり方チェックが分からない
			con.setAutoCommit(false);
			final String sql1="insert into items values(?,?,?,?,?,false)";
			final String sql2=" insert into stocks valuse (?,?,false)";
			try(PreparedStatement st1=con.prepareStatement(sql1);
					PreparedStatement st2=con.prepareStatement(sql2);	){
				st1.setString(1, itemsItemId);
				st1.setString(2, itemName);
				st1.setString(3, itemUrl);
				st1.setString(4, itemSize);
				st1.setInt(5, price);
				st2.setString(1, StocksItemId);
				st2.setInt(2, quantity);
				st1.executeUpdate();
				st2.executeUpdate();
				con.commit();
			}catch(SQLException e) {
				con.rollback();
				e.printStackTrace();
				throw e;
			}
		}
	}
}

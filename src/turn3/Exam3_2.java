package turn3;

import java.sql.Connection;
import java.sql.SQLException;

import turn2.YasuiDAO;

public class Exam3_2 {

	public static void main(String[] args) {
		String userId="C0000001";
		String itemId="00000001";
		int quantity=3;
		YasuiDAO dao=new YasuiDAO();

		try(Connection con = dao.connect();){
			dao.processOrder(con, userId, itemId, quantity);
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

}
